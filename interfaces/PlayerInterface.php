<?php
namespace interfaces;
interface PlayerInterface {

    public function Attack();
    public function Defense();
    public function Keeping();
}