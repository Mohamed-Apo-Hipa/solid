<?php

require __DIR__ . '/../vendor/autoload.php';

$players = [];
for($i=1; $i<5 ;  $i++ )
{
    array_push($players, new \Classes\AttackPlayer());
     
} //end for 

for($i=1; $i<5 ;  $i++ )
{
    array_push($players, new \Classes\DefensePlayer());
    
} //end for 

    array_push($players, new \Classes\KeepingPlayer());

$match = new \Classes\Match($players);
$match->start();
